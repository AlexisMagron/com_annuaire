<?php

defined('_JEXEC') or die('Accès interdit');


//Différentes routes du composants

class AnnuaireController extends JControllerLegacy
{

    /*
     * Accéder au profil de quelqu'un
     */
    public function profil()
    {
        $view = $this->getView("profil", "html");

        $view->display();
    }

    /*
     *  Annuaire des étudiants
     */
    public function annuaire()
    {
        $view = $this->getView("annuaire", "html");


        $view->display();
    }

}

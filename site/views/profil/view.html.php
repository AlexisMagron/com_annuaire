<?php

defined('_JEXEC') or die('Accès refusé');


class AnnuaireViewProfil extends JViewLegacy
{
    public function display($tpl = null)
    {
        $input = JFactory::getApplication()->input;

        $user_id = $input->get("id");

        try
        {
            if($user_id == null)
                throw new Exception("Erreur : utilisateur introuvable");

            $db = JFactory::getDbo();

            $query_user = $db->getQuery(true);
            $query_profile = $db->getQuery(true);

            $query_user->select("*")->from("#__users")->where("id = $user_id");

            $query_profile->select("*")->from("#__user_profiles")->where("user_id = $user_id")->order("ordering");

            $db->setQuery($query_user);
            $this->user = $db->loadAssoc();

            $db->setQuery($query_profile);
            $this->profile = $db->loadAssocList();




            $this->mail = $this->user['email'];
            $this->name = $this->user['name'];


            $params = array();

            foreach($this->profile as $value)
            {
                $key = str_replace("profileannuaire.annuaire_", "", $value['profile_key']);

                $valeur = json_decode($value['profile_value']);

                $params[$key] = $valeur;
            }

            $message = "L'utilisateur n'a pas indiqué cette information";

            if(empty($params['ville']))
                $params['ville'] = $message;

            if(empty($params['pays']))
                $params['pays'] = $message;

            if(empty($params['formation']))
                $params['formation'] = $message;
            else
            {
                $query = $db->getQuery(true);
                $query->select("*")->from("#__formations")->where("id = ".$params['formation']);
                $db->setQuery($query);
                $form = $db->loadAssoc();
                $params['formation'] = $form['name'];

            }

            if(empty($params['promotion']))
                $params['promotion'] = $message;
            else
            {
                $query = $db->getQuery(true);
                $query->select("*")->from("#__promotions")->where("id = ".$params['promotion']);
                $db->setQuery($query);
                $prom = $db->loadAssoc();
                $params['promotion'] = $prom['annee_deb']." / ".$prom['annee_fin'];
            }

            if(empty($params['description']))
                $params['description'] = $message;


            $this->params = $params;

           parent::display($tpl);


        }
        catch(RuntimeException $re)
        {
            echo "Une erreur technique s'est produite";
        }
        catch(Exception $e)
        {
            echo "Erreur : " . $e->getMessage();
        }

    }
}
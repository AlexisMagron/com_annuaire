<?php

defined('_JEXEC') or die('Accès refusé');


class AnnuaireViewAnnuaire extends JViewLegacy
{


    public function display($tpl = null)
    {
        $this->variables_formulaires();

        $this->afficher_users();




        JFactory::getDocument()->addStyleSheet("media/jui/css/bootstrap.min.css");
        parent::display($tpl);
    }

    //Affiche les utilisateurs selon les parametres de recherches envoyés
    private function afficher_users()
    {

        //Si les 3 paramètres n'ont pas été indiqués, la recherche ne se fait pas
        if(
        (!isset($this->formation) or empty($this->formation)) AND
        (!isset($this->promotion) or empty($this->promotion)) AND
        (!isset($this->search) or empty($this->search))
        )
            return null;


        $db = JFactory::getDbo();

        $query = $db->getQuery(true);


        //Requete de base triées par utilisateurs
        $query->select("id,name")->from("#__users")->order("name");


        //Lorsqu'un nom est recherché
        if(isset($this->search) && !empty($this->search))
        {

            $noms = explode(" ", $this->search);

            foreach($noms as $nom)
            {
                $query->where("name LIKE '%$nom%'");
            }
        }

        //Pour filtrer avec la formation
        if(isset($this->formation) && !empty($this->formation))
        {
            $query_form = $db->getQuery(true);
            $query_form
                ->select('id,name')
                ->from('#__users')
                ->innerJoin("#__user_profiles on #__users.id = #__user_profiles.user_id")
                ->where("profile_key=\"profileannuaire.annuaire_formation\"")
                ->where("profile_value='\"$this->formation\"'");

            $query->where("(id,name) IN ($query_form)");

        }

        //Pour filtrer avec la promotion
        if(isset($this->promotion) && !empty($this->promotion))
        {
            $query_prom = $db->getQuery(true);
            $query_prom
                ->select('id,name')
                ->from('#__users')
                ->innerJoin("#__user_profiles on #__users.id = #__user_profiles.user_id")
                ->where("profile_key=\"profileannuaire.annuaire_promotion\"")
                ->where("profile_value='\"$this->promotion\"'");

            $query->where("(id,name) IN ($query_prom)");
        }




        try
        {
            $db->setQuery($query);
            $this->users = $db->loadAssocList();
        }
        catch(Exception $e)
        {
            die($e->getMessage());
        }


    }

    //Cette fonction s'occupe de récupérer les paramètres et de définir les variables qui seront utilisées dans le template
    private function variables_formulaires()
    {
        $db = JFactory::getDbo();
        $input = JFactory::getApplication()->input;
        $query_form = $db->getQuery(true);
        $query_prom = $db->getQuery(true);

        $query_form->select("*")->from("#__formations");
        $query_prom->select("*")->from("#__promotions");

        try
        {
            $db->setQuery($query_form);

            $this->forms = $db->loadAssocList();

            $db->setQuery($query_prom);

            $this->proms = $db->loadAssocList();

            $this->search = $input->get("search", '', 'post');
            $this->formation = $input->get("formation");
            $this->promotion = $input->get("promotion");


        }
        catch(Exception $e)
        {
            die();
        }
    }
}
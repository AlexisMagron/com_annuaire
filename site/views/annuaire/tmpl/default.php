<h1>Annuaire</h1>

<div class="container">
<form class="form-inline" action="index.php?option=com_annuaire&task=annuaire" method="get">
    <input type="hidden" name="option" value="com_annuaire"/>
    <input type="hidden" name="task" value="annuaire"/>

    <h3>Rechercher un membre de l'association</h3><br />

        <input autocomplete="off" type="text" name="search" placeholder="Entrez le nom" value="<?php echo $this->search; ?>">


        <select name="formation">
            <option value="">Tous les départements</option>
            <?php foreach($this->forms as $form) : ?>
                <option <?php if($form['id'] == $this->formation) echo "selected"; ?> value="<?php echo $form['id']; ?>"><?php echo $form['name']; ?></option>
            <?php endforeach; ?>
        </select>



        <select name="promotion">
            <option value="">Toutes les périodes</option>
            <?php foreach($this->proms as $prom) : ?>
                <option <?php if($prom['id'] == $this->promotion) echo "selected"; ?> value="<?php echo $prom['id']; ?>"><?php echo $prom['annee_deb'] . " / " . $prom['annee_fin']; ?></option>
            <?php endforeach; ?>
        </select>



    <button type="submit" class="btn btn-primary">Chercher</button>
</form>

    <?php if(isset($this->users)) : ?>
        <?php foreach($this->users as $user) : ?>

            <a href="index.php?option=com_annuaire&task=profil&id=<?php echo $user['id']; ?>"><?php echo $user['name']; ?></a><br />

        <?php endforeach; ?>
    <?php endif; ?>



</div>
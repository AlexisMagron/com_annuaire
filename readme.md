#Installation
Pour installer le plugin, compresser le dépot en ZIP et ajouter ce zip via le gestionnaire d'extension

#Développement
Installer d'abord le plugin, puis supprimer /components/com_annuaire et /admin/components/com_annuaire pour les remplacer par des liens symboliques : 
/dossier-du-projet/site -> components/com_annuaire
/dossier-du-projet/admin -> components/com_annuaire

Travailler dans le dossier du projet


#Documentation
https://docs.joomla.org/J3.x:Developing_a_MVC_Component/Introduction

<?php

defined('_JEXEC') or die('Accès refusé');


class AnnuaireViewFormations extends JViewLegacy
{

    function display($tpl = null)
    {
        //Récupère les formations
        $this->items = $this->get('Items');
        $this->pagination = $this->get('Pagination');


        $this->addToolBar();


        parent::display($tpl);

    }

    function addToolBar()
    {
        JToolbarHelper::title(JText::_("Gestion des formations"));

        JToolbarHelper::addNew("formation.add");
        JToolbarHelper::editList("formation.edit");
        JToolbarHelper::deleteList("Etes vous sur ?","formations.delete");
    }

}
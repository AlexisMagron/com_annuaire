<?php

defined('_JEXEC') or die;
?>



<form action="index.php?option=com_annuaire&view=formations" method="post" id="adminForm" name="adminForm">

    <table class="table table-striped table-hover">
        <thead>
            <tr>
                <th width="4%"><?php echo JHtml::_('grid.checkall') ;?></th>
                <th width="4%">ID</th>
                <th>Département</th>
            </tr>
        </thead>

        <tfoot>
            <tr>
                <td colspan="5"><?php echo $this->pagination->getListFooter(); ?></td>
            </tr>
        </tfoot>


        <tbody>
            <?php if(!empty($this->items)) : ?>
                <?php foreach($this->items as $i => $row) : ?>

                <tr>
                    <td>
                        <?php echo JHtml::_('grid.id', $i, $row->id);?>
                    </td>

                    <td>
                        <?php echo $row->id; ?>
                    </td>

                    <td>
                        <a href="index.php?option=com_annuaire&view=formation&layout=edit&id=<?php echo $row->id; ?>">
                            <?php echo $row->name; ?>
                        </a>
                    </td>

                </tr>

            <?php endforeach; ?>
        <?php endif ; ?>


        </tbody>



    </table>

    <input type="hidden" name="task" value="" />
    <input type="hidden" name="boxchecked" value="0" />
    <?php echo JHtml::_('form.token'); ?>

</form>





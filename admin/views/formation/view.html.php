<?php

defined('_JEXEC') or die;

class AnnuaireViewFormation extends JViewLegacy
{
    protected $form;
    protected $item;

    public function display($tpl = null)
    {
        $this->form = $this->get('Form');
        $this->item = $this->get('Item');



        if (count($errors = $this->get('Errors')))
        {
            JError::raiseError(500, implode('<br />', $errors));

            return false;
        }


        $this->addToolBar();

        $this->setLayout("edit");
        parent::display($tpl);
    }

    protected function addToolBar()
    {
        $input = JFactory::getApplication()->input;

        $is_new = ($this->item->id == 0);

        if($is_new)
            $title = "Ajouter une formation";
        else
            $title = "Modifier une formation";

        JToolbarHelper::title($title);
        JToolbarHelper::save('formation.save');
        JToolbarHelper::cancel("formation.cancel",
            $is_new ? 'JTOOLBAR_CANCEL' : 'JTOOLBAR_CLOSE'
        );




    }
}
<?php

defined('_JEXEC') or die;

class AnnuaireViewPromotion extends JViewLegacy
{
    protected $form;
    protected $item;

    public function display($tpl = null)
    {
        $this->form = $this->get('Form');
        $this->item = $this->get('Item');



        if (count($errors = $this->get('Errors')))
        {
            JError::raiseError(500, implode('<br />', $errors));

            return false;
        }


        $this->addToolBar();

        $this->setLayout("edit");
        parent::display($tpl);
    }

    protected function addToolBar()
    {
        $input = JFactory::getApplication()->input;

        $is_new = ($this->item->id == 0);

        if($is_new)
            $title = "Ajouter une promotion";
        else
            $title = "Modifier une promotion";

        JToolbarHelper::title($title);
        JToolbarHelper::save('promotion.save');
        JToolbarHelper::cancel("promotion.cancel",
            $is_new ? 'JTOOLBAR_CANCEL' : 'JTOOLBAR_CLOSE'
        );




    }
}
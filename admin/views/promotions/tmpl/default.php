<?php

defined('_JEXEC') or die;
?>



<form action="index.php?option=com_annuaire&view=promotions" method="post" id="adminForm" name="adminForm">

    <table class="table table-striped table-hover">
        <thead>
        <tr>
            <th width="4%"><?php echo JHtml::_('grid.checkall') ;?></th>
            <th width="4%">ID</th>
            <th>Date de la promotion</th>
        </tr>
        </thead>

        <tfoot>
        <tr>
            <td colspan="5"><?php echo $this->pagination->getListFooter(); ?></td>
        </tr>
        </tfoot>


        <tbody>
        <?php if(!empty($this->items)) : ?>
            <?php foreach($this->items as $i => $row) : ?>

                <tr>
                    <td>
                        <?php echo JHtml::_('grid.id', $i, $row->id);?>
                    </td>

                    <td>
                        <?php echo $row->id; ?>
                    </td>

                    <td>
                        <a href="index.php?option=com_annuaire&view=promotion&layout=edit&id=<?php echo $row->id;?>">
                        <?php echo "$row->annee_deb / $row->annee_fin"; ?>

                        </a>

                    </td>

                </tr>

            <?php endforeach; ?>
        <?php endif ; ?>


        </tbody>



    </table>

    <input type="hidden" name="task" value="" />
    <input type="hidden" name="boxchecked" value="0" />
    <?php echo JHtml::_('form.token'); ?>

</form>





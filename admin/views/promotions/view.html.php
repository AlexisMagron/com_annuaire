<?php

defined('_JEXEC') or die('Accès refusé');


class AnnuaireViewPromotions extends JViewLegacy
{

    function display($tpl = null)
    {
        //Récupère les formations
        $this->items = $this->get('Items');
        $this->pagination = $this->get('Pagination');


        $this->addToolBar();


        parent::display($tpl);

    }

    function addToolBar()
    {
        JToolbarHelper::title(JText::_("Gestion des promotions"));

        JToolbarHelper::addNew("promotion.add");
        JToolbarHelper::editList("promotion.edit");
        JToolbarHelper::deleteList("Etes vous sur ?","promotions.delete");
    }

}
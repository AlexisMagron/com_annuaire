<?php

defined('_JEXEC') or die('Accès interdit');


//Différentes routes du composants

class AnnuaireController extends JControllerAdmin
{
    public function display( $cachable = false, $urlparams = array())
    {

        $input = JFactory::getApplication()->input;
        $view_name = $input->get('view');

        if(empty($view_name))
            $view_name = $input->get('task');

        if(!empty($view_name))
        {
            $model = $this->getModel($view_name);


            $view = $this->getView($view_name, "html");

            $view->setModel($model, true);

            $view->display();
        }
        else
        {
            //TODO : afficher un menu pour rediriger vers formations, promotions
        }



        return $this;
    }




}

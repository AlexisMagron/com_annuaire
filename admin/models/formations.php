<?php

defined('_JEXEC') or die;

class AnnuaireModelFormations extends JModelList
{
    public function getListQuery()
    {

        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $query->select('*')->from($db->quoteName("#__formations"));



        return $query;
    }

}
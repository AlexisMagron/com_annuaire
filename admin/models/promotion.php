<?php

defined('_JEXEC') or die;

class AnnuaireModelPromotion extends JModelAdmin
{
    public function getTable($type="Promotion", $prefix = "AnnuaireTable", $config=array())
    {
        return JTable::getInstance($type, $prefix, $config);
    }

    /**
     * Abstract method for getting the form from the model.
     *
     * @param   array $data Data for the form.
     * @param   boolean $loadData True if the form is to load its own data (default case), false if not.
     *
     * @return  mixed  A JForm object on success, false on failure
     *
     * @since   12.2
     */
    public function getForm($data = array(), $loadData = true)
    {

        $form = $this->loadForm(
            "com_annuaire.promotion",
            "promotion",
            array(
                'control' => 'jform',
                'load_data' => $loadData
            )
        );

        if(empty($form))
            return false;

        return $form;
    }

    public function loadFormData()
    {
        $data = JFactory::getApplication()->getUserState(
            'com_annuaire.edit.promotion.data',
            array()
        );

        if(empty($data))
        {
            $data = $this->getItem();
        }

        return $data;
    }


}